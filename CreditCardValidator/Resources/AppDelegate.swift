import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        if let window = window {
            let creditCardValidatorController = CreditCardValidatorController()
            window.rootViewController = creditCardValidatorController

            application.statusBarStyle = UIStatusBarStyle.lightContent
            window.makeKeyAndVisible()
        }

        return true
    }
}
