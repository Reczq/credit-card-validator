Create an application that will validate credit card numbers.
Requirements:
Create drop-in control for credit card input that will behave similar like on video: https://goo.gl/ULeihV
Place in the view:
Control created in point 1
Validate button
Validity indication
Generate button
UI Requirements
Do not use Storyboards/xib files
Support only iPhones
Validate cards using bincodes service. Api key: 5232a9bca11e25c0f8eb4313ff2644be
Implement credit card number generator
Using external libraries is forbidden
Write tests
Fetching the data cannot block the UI.
Candidate can use any framework he/she thinks might help finishing the application, excluding the use of networking frameworks.
The application needs to be ready for deployment (no private API’s, etc.).
Please target iOS 9.0+.
Use Autolayout.
Please send back in zip.
